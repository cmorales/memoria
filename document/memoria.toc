\select@language {spanish}
\select@language {spanish}
\select@language {spanish}
\contentsline {chapter}{Resumen}{\es@scroman {vii}}{chapter*.1}
\contentsline {chapter}{Abstract}{\es@scroman {ix}}{chapter*.2}
\select@language {spanish}
\contentsline {chapter}{Tabla de Contenidos}{\es@scroman {xi}}{chapter*.3}
\contentsline {chapter}{Tabla de Figuras}{\es@scroman {xvii}}{chapter*.4}
\contentsline {chapter}{Tabla de C\IeC {\'o}digos Fuentes}{\es@scroman {xix}}{chapter*.5}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Internet de las cosas}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}M2M}{2}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Desarrollo de aplicaciones M2M}{6}{subsection.1.1.2}
\contentsline {section}{\numberline {1.2}Planteamiento del problema}{8}{section.1.2}
\contentsline {section}{\numberline {1.3}Soluci\IeC {\'o}n propuesta y objetivos}{9}{section.1.3}
\contentsline {section}{\numberline {1.4}Estructura del documento}{10}{section.1.4}
\contentsline {chapter}{\numberline {2}Estado del Arte}{13}{chapter.2}
\contentsline {section}{\numberline {2.1}Sistemas de comunicaci\IeC {\'o}n M2M}{13}{section.2.1}
\contentsline {section}{\numberline {2.2}Protocolos de comunicaci\IeC {\'o}n M2M}{16}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Protocolo TCP}{17}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Protocolos M2M}{17}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Aplicaciones de red de alto rendimiento}{18}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Operaciones I/O as\IeC {\'\i }ncronas}{18}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Java}{19}{subsection.2.3.2}
\contentsline {subsubsection}{Java NIO}{21}{section*.9}
\contentsline {section}{\numberline {2.4}Arquitectura orientada a servicios (SOA)}{24}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Tipos de arquitecturas SOA}{25}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Herramientas de tipo SOA}{26}{subsection.2.4.2}
\contentsline {chapter}{\numberline {3}Arquitectura de la soluci\IeC {\'o}n}{29}{chapter.3}
\contentsline {section}{\numberline {3.1}Objetivos y atributos de calidad del sistema}{30}{section.3.1}
\contentsline {section}{\numberline {3.2}Estilo de arquitectura cliente-servidor}{31}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Sistema de comunicaci\IeC {\'o}n cliente-servidor}{32}{subsection.3.2.1}
\contentsline {section}{\numberline {3.3}Estilo de arquitectura orientada a servicios}{33}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Conectores usados en SOA}{34}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Sistema de comunicaci\IeC {\'o}n orientado a servicios}{35}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Arquitectura sistema de comunicaci\IeC {\'o}n M2M}{36}{section.3.4}
\contentsline {chapter}{\numberline {4}Framework para el desarrollo y gesti\IeC {\'o}n de m\IeC {\'o}dulos}{39}{chapter.4}
\contentsline {section}{\numberline {4.1}Gesti\IeC {\'o}n de m\IeC {\'o}dulos}{39}{section.4.1}
\contentsline {section}{\numberline {4.2}Implementaci\IeC {\'o}n}{40}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Lenguaje de programaci\IeC {\'o}n}{41}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Dise\IeC {\~n}o basado en capas}{41}{subsection.4.2.2}
\contentsline {subsubsection}{Capa de m\IeC {\'o}dulo con estados}{42}{section*.17}
\contentsline {subsubsection}{Capa de m\IeC {\'o}dulo autoconfigurable}{45}{section*.20}
\contentsline {subsubsection}{Capa de entorno modular}{46}{section*.22}
\contentsline {subsubsection}{Capa aplicativa}{47}{section*.23}
\contentsline {chapter}{\numberline {5}M\IeC {\'o}dulo de mensajer\IeC {\'\i }a}{49}{chapter.5}
\contentsline {section}{\numberline {5.1}Modelo de comunicaci\IeC {\'o}n as\IeC {\'\i }ncrona}{49}{section.5.1}
\contentsline {section}{\numberline {5.2}Desarrollo}{50}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Tipos de comunicaci\IeC {\'o}n as\IeC {\'\i }ncrona}{51}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Persistencia de mensajes}{52}{subsection.5.2.2}
\contentsline {section}{\numberline {5.3}Implementaci\IeC {\'o}n}{53}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Configuraci\IeC {\'o}n del M\IeC {\'o}dulo}{53}{subsection.5.3.1}
\contentsline {chapter}{\numberline {6}M\IeC {\'o}dulo de comunicaci\IeC {\'o}n}{55}{chapter.6}
\contentsline {section}{\numberline {6.1}Modelo de implementaci\IeC {\'o}n}{55}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Protocolos de comunicaci\IeC {\'o}n}{56}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Interfaz del dispositivo}{57}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}Interfaz de servicios}{58}{subsection.6.1.3}
\contentsline {section}{\numberline {6.2}Desarrollo}{59}{section.6.2}
\contentsline {section}{\numberline {6.3}Modelo de controladores de canales}{60}{section.6.3}
\contentsline {section}{\numberline {6.4}Implementaci\IeC {\'o}n}{62}{section.6.4}
\contentsline {chapter}{\numberline {7}M\IeC {\'o}dulo consumidor de servicios}{63}{chapter.7}
\contentsline {section}{\numberline {7.1}Modelo de implementaci\IeC {\'o}n}{63}{section.7.1}
\contentsline {section}{\numberline {7.2}Desarrollo}{64}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Implementaci\IeC {\'o}n}{65}{subsection.7.2.1}
\contentsline {chapter}{\numberline {8}Validaci\IeC {\'o}n y conclusiones}{67}{chapter.8}
\contentsline {section}{\numberline {8.1}Validaci\IeC {\'o}n}{67}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}Extensibilidad}{67}{subsection.8.1.1}
\contentsline {subsubsection}{Resultados}{68}{section*.28}
\contentsline {subsection}{\numberline {8.1.2}Escalabilidad}{69}{subsection.8.1.2}
\contentsline {subsubsection}{Prueba}{69}{section*.29}
\contentsline {subsubsection}{Resultados}{70}{section*.30}
\contentsline {section}{\numberline {8.2}Conclusiones generales}{73}{section.8.2}
\contentsline {section}{\numberline {8.3}Trabajo futuro}{75}{section.8.3}
\contentsline {chapter}{Bibliograf\IeC {\'\i }a}{77}{chapter*.34}
\contentsline {chapter}{Glosario}{86}{chapter*.35}
\contentsline {chapter}{\numberline {A}Manual de uso}{87}{appendix.Alph1}
\contentsline {section}{\numberline {A.1}Introducci\IeC {\'o}n}{87}{section.Alph1.1}
\contentsline {section}{\numberline {A.2}Implementando un sistema de ejemplo}{88}{section.Alph1.2}
\contentsline {subsection}{\numberline {A.2.1}Creando entorno de trabajo}{88}{subsection.Alph1.2.1}
\contentsline {subsection}{\numberline {A.2.2}M\IeC {\'o}dulos y atributos gen\IeC {\'e}ricos}{90}{subsection.Alph1.2.2}
\contentsline {subsection}{\numberline {A.2.3}Coordinaci\IeC {\'o}n local de m\IeC {\'o}dulos}{91}{subsection.Alph1.2.3}
\contentsline {subsection}{\numberline {A.2.4}Implementando el m\IeC {\'o}dulo de comunicaci\IeC {\'o}n}{92}{subsection.Alph1.2.4}
\contentsline {subsubsection}{Implementando decodificadores, codificadores y analizadores}{93}{section*.36}
\contentsline {subsubsection}{Definiendo interfaz de dispositivo}{96}{section*.37}
\contentsline {subsubsection}{Definiendo interfaz de servicios}{98}{section*.38}
\contentsline {subsection}{\numberline {A.2.5}Implementando el m\IeC {\'o}dulo de mensajer\IeC {\'\i }a}{102}{subsection.Alph1.2.5}
\contentsline {subsection}{\numberline {A.2.6}Implementando el m\IeC {\'o}dulo consumidor de servicios}{102}{subsection.Alph1.2.6}
\contentsline {section}{\numberline {A.3}Ejecutando el sistema}{104}{section.Alph1.3}
