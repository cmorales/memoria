\chapter{Validación y conclusiones}\label{ch:conclusiones}

Este capítulo describe los enfoques y los procedimientos utilizados para validar los objetivos del proyecto desarrollado, además de las conclusiones obtenidas mediante ellos. También se abordarán aquellos tópicos pendientes que deben ser desarrollados en el futuro para mejorar y ampliar las características actuales del sistema.

\section{Validación}

Es posible analizar el proyecto desarrollado desde diversos enfoques relacionados con los objetivos planteados en el capítulo 1. Cada uno de ellos será abordado a continuación.

\subsection{Extensibilidad}

Desde su desarrollo, se han realizado dos implementaciones de la plataforma, ambas enfocadas en el desarrollo de aplicaciones demostrativas o prototipos. Esto si bien no valida la implementación a través del funcionamiento constante y dedicado, permite comprobar su enorme potencial incluso en etapas tempranas del desarrollo de \textit{software}.

La primera de estas aplicaciones tiene como finalidad la medición de variables agroclimáticas obtenidas mediante dispositivos electrónicos distribuidos, conectados a través de Internet, los cuales transmiten estos datos hacia un servidor central, el cual también gestiona su funcionamiento.

La otra tiene por objetivo la administración remota de personal de limpieza a través de \textit{tablets}, los cuales cuentan con un lector de huellas dactilares que permite administrar sus ingresos y salidas y reportarlos de manera inalámbrica hacia un sistema centralizado que permite obtener variables de desempeño del personal.

\subsubsection{Resultados}

Desde el punto de vista de la compatibilidad de la comunicación, el primer ejemplo se trata de un protocolo propietario basado en mensajes en texto a través del protocolo TCP/IP, el cual se adaptó sin problema al modelo de comunicación definido por el sistema de comunicación, el cual estaba alojado en una máquina servidor, al igual que las rutinas de procesamiento de datos. Además fue posible validar no solo la obtención de datos, si no también la gestión de su ciclo de vida en base a las herramientas entregadas por la plataforma.

La segunda implementación fue desarrollada en base a un protocolo TCP/IP desarrollado especialmente para el proyecto, por lo que se desarrolló el protocolo tomando en cuenta las consideraciones propias de la plataforma.

En ambos ejemplos la implementación solo requirió de la configuración de las propiedades del sistema y del protocolo de comunicación, lo cual tomó considerablemente menos tiempo en comparación al desarrollo de la lógica de comunicación y de procesamiento de datos, agilizando la puesta en marcha de los proyectos mencionados.

Estas experiencias permiten validar la propiedad de extensibilidad del sistema, y su compatibilidad con aquellos dispositivos M2M que utilizan el protocolo TCP/IP.

\subsection{Escalabilidad}

La escalabilidad se refiere a aquella propiedad de los sistemas informáticos de adaptarse y reaccionar frente al crecimiento continuo de trabajo de manera fluida, sin perder calidad.

Esta característica se validó mediante la utilización de equipos simulados a través de un script desarrollado en el lenguaje de programación Python \cite{van1995python}, el cual permite implementar múltiples hebras que enviarán mensajes a la plataforma, con el fin de probar su escalabilidad. Este simulador permite configurar diversos aspectos de comunicación, incluyendo el número de hebras, el tiempo entre cada mensaje, el momento de iniciar la comunicación, entre otros.

\subsubsection{Prueba}

La prueba constará de 800 equipos simulados conectados simultáneamente durante 15 minutos.

Cada hebra enviará dos tipos de mensajes, uno al iniciar la conexión y otro cada 30 segundos, el cual contiene el tiempo en el que el mensaje fue creado. Cada una comenzará su rutina de envío de mensajes luego de esperar un tiempo aleatorio definido dentro de un rango de 30 segundos, lo cual evitará que todas las hebras envíen los mensajes en instantes similares, haciendo más realista el escenario de pruebas.

La implementación del sistema de comunicación utilizado para este ejemplo está detallada a modo de manual en el apéndice A, para la cual se ocupará una arquitectura compuesta por un módulo de comunicación, un módulo de mensajería y un módulo consumidor de servicios, dispuestos en un mismo equipo. El \textit{script} en Python correrá en un servidor independiente.

Los resultados fueron obtenidos a través de la utilización de la herramienta NetBeans Profiler \cite{profiler2014version}, la cual permite analizar el uso de CPU, memoria, hebras y otro tipos de parámetros.

El equipo utilizado posee las siguientes características:
\begin{itemize}
	\item \textbf{Procesador} Intel Core i7-3630QM CPU, 2.40GHz x 8.
	\item \textbf{Memoria RAM} 4 GB.
	\item \textbf{Sistema operativo} Ubuntu 14.04 LTS, 64 bit.
	\item \textbf{Tamaño mínimo de memoria} de la Java Virtual Machine 128MB.
	\item \textbf{Tamaño máximo de memoria} de la Java Virtual Machine 1GB.
	\item \textbf{Tamaño máximo de descriptores de archivos} 20000.
\end{itemize}

\subsubsection{Resultados}

El gráfico de la figura~\ref{fig:100_memory_heap} muestra el uso de memoria de la plataforma durante la prueba. Se puede apreciar que durante los primeros minutos el mínimo de la memoria asignada corresponde a 128 MB, de los cuales solo una fracción es ocupada. Una vez iniciados los 800 equipos simulados la memoria asignada sube progresivamente, llegando a un límite de 473 MB asignados, de los cuales 413 MB fueron ocupados. 

En la gráfica el crecimiento y disminución de la memoria utilizada se debe al trabajo realizado por el recolector de basura, aplicación de la JVM que iterativamente elimina la memoria sin utilizar. Según los datos del gráfico, al comenzar la conexión de los 800 equipos la frecuencia del funcionamiento del recolector aumentan considerablemente, junto con el aumento de la memoria disponible por el sistema.

A medida que la prueba es ejecutada la memoria total y la utilizada disminuyen progresivamente, lo que refleja la estabilidad de la plataforma frente a un ambiente de alta concurrencia de datos. Además se aprecia que no fue necesario ocupar el máximo de memoria configurada para la JVM (1 GB.).

\begin{figure}[h!]
   \centering
  	\includegraphics[width=\linewidth]{../images/800_memory_heap.png}
   \caption{Gráfico que muestra la cantidad de memoria máxima asignada y la cantidad de memoria utilizada a lo largo de la prueba.}
   \label{fig:100_memory_heap}
\end{figure}

El siguiente gráfico~\ref{fig:800_memory_GC} corresponde al de generaciones sobrevivientes. Dentro de esta métrica una generación corresponde a un conjunto de instancias creadas dentro del mismo ciclo entre recolecciones de basura. Una generación sobreviviente es aquella que existe aun después de que el colector de basura hizo su labor. Este gráfico señala la cantidad de generaciones sobrevivientes que aún están vivas en memoria. Típicamente estas generaciones corresponden a objetos de larga vida dentro de la aplicación. Es de esperar que el número de generaciones sobrevivientes se mantenga estable durante el funcionamiento, lo que descartaría la presencia de filtraciones de memoria que eventualmente desencadenarían problemas en la aplicación.

\begin{figure}[h!]
   \centering
  	\includegraphics[width=\linewidth]{../images/800_memory_GC.png}
   \caption{Gráfico que muestra la cantidad de generaciones sobrevivientes a la ejecución del colector de basura de la máquina virtual de Java.}
   \label{fig:800_memory_GC}
\end{figure}

Analizando esta información se puede concluir que el número de generaciones sobrevivientes se mantiene estable durante la ejecución del sistema, a pesar de un leve aumento al final de la prueba. En base a lo señalado se descarta la presencia de una filtración mayor de memoria, sin embargo se deberá analizar detalladamente este punto en el futuro.

El gráfico de la figura~\ref{fig:800_memory_threads} representa la cantidad de hebras creadas durante la ejecución de la prueba, junto al número total de instancias creadas por el sistema. 

\begin{figure}[h!]
   \centering
  	\includegraphics[width=\linewidth]{../images/800_memory_threads.png}
   \caption{Gráfico que muestra la cantidad de hebras utilizadas y de instancias creadas a lo largo de la ejecución de la prueba.}
   \label{fig:800_memory_threads}
\end{figure}

Analizando esta métrica, se puede apreciar una cima en el número de hebras en el inicio de la prueba, que luego disminuye y se mantiene estable durante todo el funcionamiento. Además, luego de los 15 minutos de funcionamiento los equipos comienzan a desconectarse de manera iterativa, lo que explica la disminución gradual de las hebras utilizadas por la plataforma luego de ese periodo de tiempo.

Esta estabilidad en el número de hebras, sumado a la estabilidad de las otras variables analizadas demuestra el buen manejo de la solución desarrollada frente a un alto número de conexiones concurrentes.

\section{Conclusiones generales}

A lo largo de esta memoria se han analizado los distintos aspectos relacionados con el desarrollo de una plataforma de comunicación compatible con dispositivos M2M, cuyos principales requerimientos fueron planteados en el capítulo 1. El concepto detrás del proyecto desarrollado es la reutilización de código, es decir crear una herramienta que posea las consideraciones que todos los proyectos M2M presentan, con el fin de ahorrar recursos en su desarrollo. El primer gran reto, por lo tanto, es la definición de un modelo genérico, que permitiese adaptarse a la mayoría de los sistemas conocidos.

La definición de un modelo de abstracción para una aplicación debe tener como base la definición de los requerimientos o atributos identificados en el sistema. Cualquiera sea el proyecto desarrollado, sus atributos deben ser identificados en base a la experiencia, y es labor del diseñador de \textit{software} saber identificar aquellos requerimientos de manera temprana, ya que siempre resulta mucho más costoso considerarlos en etapas posteriores. Esta labor no es tarea sencilla, sobre todo cuando no se dispone del tiempo necesario para realizarlo, algo muy frecuente en la industria del desarrollo del \textit{software}, en donde los tiempos nunca son holgados. La primera conclusión del trabajo realizado es que, incluso más importante que el desarrollo mismo, es el correcto diseño de la solución, tanto a nivel de arquitectura de \textit{software}, como de diseño de clases. Un correcto diseño es aquel que es capaz de entregar una serie de atributos al sistema, los cuales deben ser certeramente identificados.

Una vez identificada la forma de la solución que se quiere desarrollar, es importante investigar y aprender sobre los aspectos más importantes a los que la aplicación se ve enfrentada bajo la problemática a resolver. Bajo ese contexto se puede concluir que existe una inmensa cantidad de herramientas disponibles distribuidas a través del modelo de código abierto, que permiten solucionar una amplia gama de problemas relacionados con la informática y con cualquier disciplina en general. Esto permite a la comunidad tener acceso gratuito a tecnología de alto nivel, permitiendo a los desarrolladores agilizar la construcción de proyectos sin dejar de lado la calidad. No aprovechar los esfuerzos comunitarios resulta un error en la época actual.

Dentro de las tecnológicas utilizadas para el desarrollo de sistemas de comunicación como los involucrados en el presente trabajo, destacan aquellas que permiten la implementación de sistemas de comunicación asíncrona. Estas técnicas permiten aprovechar al máximo los recursos de \textit{hardware}, minimizando la cantidad de operaciones I/O bloqueantes. De esta manera se puede utilizar el ancho de banda de comunicación a su máxima capacidad. Este modelo fue implementados en dos niveles de diseño, primero a través de la tecnología Java NIO que permite el acceso no bloqueante a los canales de comunicación, y luego en el modelo de comunicación utilizado para comunicar el módulo de comunicación con el consumidor de servicios.

La plataforma tiene por objetivo proveer a los desarrolladores de aplicaciones M2M de herramientas que puedan adaptarse a tecnologías M2M, bajo una serie de consideraciones técnicas definidas en los primeros capítulos. En base a las distintas implementaciones realizadas se puede concluir que, más importante que las propiedades que presenta el \textit{software}, es su adecuada implementación. En ese contexto es importante recalcar lo fundamental que resulta, en base a las posibilidades, la definición de un adecuado protocolo de comunicación, enfocado en la mitigación de bloqueos mutuos, frecuentes en sistemas de comunicación que utilizan técnicas asíncrona.

\section{Trabajo futuro}

La principal característica que debe ser mejorada por la plataforma tiene que ver con la extensibilidad, principalmente con la compatibilidad con los protocolos de comunicación pertenecientes a la familia TCP/IP. Actualmente está acotada a los dispositivos que se comuniquen a través del protocolo TCP. Esta propiedad es fundamental dentro de la plataforma construida, por lo que nuevos protocolos deben ser soportados. Cuáles deben ser soportados debe ser definido en base a los requerimientos de algún dispositivo en específico y de las alternativas que Netty posee. 

Si bien el trabajo presentado presenta una arquitectura sólida, a nivel de desarrollo aún es posible realizar algunas optimizaciones. Entre ellas destacan la necesidad del diseño de un nuevo modelo de inyección de módulos en la capa aplicativa del \textit{framework} para la gestión de módulos. Actualmente el gestor de módulos de un entorno tiene definido los tipos de módulos soportados a nivel de código.

La escalabilidad de la plataforma debe ser estudiada con mayor detalle, procurando identificar posibles pérdidas de memoria, lo que eventualmente puede provocar el problemas en su funcionamiento, sobre todo durante largos periodos de funcionamiento.

Además es necesario migrar el sistema a la versión 4.0.2 de la biblioteca Netty. Actualmente se utiliza en su versión 3.5.6.


