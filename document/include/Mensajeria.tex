\chapter{Módulo de mensajería} \label{ch:mensajeria}

El módulo de mensajería corresponde a uno de los componentes especializados del estilo SOA debido a que, como se definió en el capítulo 3, su presencia no es esencial en este tipo de sistemas, siendo posible diseñar arquitecturas que utilizan comunicación directa entre los servicios. Sin embargo el enfoque que utiliza un sistema mensajería como mediador para su comunicación presenta comparativamente más ventajas.

El presente capítulo tiene por objetivo describir la herramienta de mensajería que utilizará el sistema a desarrollar.

\section{Modelo de comunicación asíncrona}

La utilización de un sistema de mensajería permite implementar un modelo de comunicación asíncrona, el cual no requiere que tanto proveedor como consumidor dependa del otro para funcionar, permitiendo que estos puedan funcionar en computadores distintos. Estas características facilitan el alto rendimiento en sistemas desacoplados. Sin embargo este enfoque requiere de recursos adicionales necesarios para manejar las colas de mensajes, y puede provocar problemas de bloqueos mutuos en la comunicación.

A pesar de estas desventajas, diseñar sistemas de manera asíncrona permite aprovechar al máximo los recursos de \textit{hardware}, ya que con ellos se minimiza la cantidad de hilos que bloquean las operaciones I/O, permitiendo utilizar su ancho de banda a su máxima capacidad. Con un enfoque de comunicación directa cada módulo tiene que esperar una respuesta de cada solicitud que haga a otro, teniendo que esperar además el tiempo en procesamiento y la latencia de la red. Con un sistema asíncrono la comunicación entre módulos está limitado solo por el ancho de banda de red y no por la latencia.

\section{Desarrollo}

En el capítulo 2 se estudiaron aplicaciones relacionadas con el alto rendimiento y también aquellas que permiten la implementación de SOA. Dentro de estas últimas destacan las aplicaciones tipo ESB (del inglés Enterprise Service Bus) cuyo núcleo a menudo está formado por aplicaciones de mensajería o MOM, lo que permite desarrollar sistemas disociados heterogéneos, capaces de crecer y adaptarse con mayor facilidad. También permite una mayor flexibilidad al añadir o retirar elementos al sistema.

Si bien las herramientas ESB son potentes proveedores de arquitectura SOA, su utilización implica adaptar el funcionamiento de todo el sistema a los modelos de configuraciones y definiciones planteados por la aplicación ESB particular, problema similar al presentado en el capítulo 4 con los inyectores de dependencias utilizados como administradores de módulos. A pesar de que la implementación del sistema de comunicación está hecho en base a múltiples herramientas, la idea es coordinarlas de manera transparente y modular, pudiendo estas ser reemplazadas por nuevas tecnologías sin la necesidad de modificar el resto del sistema. Adoptar una aplicación del tipo ESB iría en contra de esa premisa. En cambio utilizar una aplicación de tipo MOM permitiría aprovechar todas las ventajas que un ESB puede otorgar, garantizando su modularidad y mantenimiento.

La aplicación MOM utilizada por el sistema corresponderá a HornetQ \cite{hornetq}, desarrollada por la compañía JBoss. Corresponde a un sistema de mensajería desarrollado bajo el modelo de código abierto, basado Java y que precisamente utiliza Netty \cite{netty2014} como capa de abstracción de las operaciones I/O para garantizar el alto rendimiento.

Además del hecho de implementar un sistema de comunicación asíncrona, HornetQ ofrece alta disponibilidad en la capa de \textit{software}, basado en un sistema de tolerancia a fallos (en inglés \textit{failover}) automático lo que garantiza que ningún mensaje se pierda o se duplique en caso de fallo del servidor. 

También posibilita la implementación de sistemas coordinados como clúster de servidores, los cuales podrán realizar balanceo de carga al consumir los mensajes. Estos servidores pueden estar geográficamente distantes, accediendo a los mensajes a través de internet. Esto permite a su sistema escalar horizontalmente, añadiendo de forma transparente nuevos servidores al clúster.

\subsection{Tipos de comunicación asíncrona}

HornetQ, y la mayoría de las aplicaciones MOM, permiten la definición de dos tipos de enfoques clásicos para el consumo de mensajes, el patrón cola de mensajes y el patrón publicador/subscriptor. 

El patrón cola de mensajes es aquel en donde el mensaje queda persistentemente guardado para garantizar su correcta entrega, y luego (un poco más tarde) el sistema de mensajería entrega el mensaje a un consumidor. Bajo este enfoque pueden haber muchos consumidores en la cola, pero cada mensaje será consumido por solo uno de ellos.

Por otra parte el patrón publicador/subscriptor se caracteriza porque el publicador envía un mensaje a una entidad conocida como tópico (en inglés \textit{topic}), el cual puede estar vinculado a muchos suscriptores, cada uno de los cuales recibe una copia de los mensajes enviado en ese tópico.

HornetQ permite la implementación de estos dos modelos de mensajería, basados en el interfaz de Java conocido como Java Message Service (JMS). Pero además provee lo que denomina HornetQ core API, que corresponde a un modelo de comunicación que no depende de JMS, pero que proporciona todas sus funcionalidad sin gran parte de la complejidad. También proporciona características que no están disponibles utilizando JMS. Funciona a través de un servidor que se encarga de vincular una dirección con una o más colas. Cuando se enruta un mensaje este se dirige al conjunto de colas en la dirección del mensaje. Cada cola puede ser accedida con un filtro de mensaje opcional, lo que permite que un mensaje sólo sea enviado al subconjunto de colas unidas que coinciden con esa expresión de filtro.

La API de HornetQ Core no distingue entre colas o tópicos, su modelo se basa simplemente en direcciones y colas. Bajo este paradigma es posible replicar el modelo publicador/subscriptor implementando una única dirección la cual puede estar vinculada a múltiples colas. Debido a esta facilidad conceptual se utilizará este modelo de mensajería para el desarrollo de la plataforma.

\subsection{Persistencia de mensajes}

Para almacenar los mensajes HornetQ ocupa \textit{journals} de alto rendimeinto, un registro cronológico de operaciones de  procesamiento de datos, administrado por un motor de persistencia altamente optimizado para casos de uso de mensajería específicos \cite{hornetq}.

Un \textit{journal} consiste en un conjunto de archivos pre-creados con un tamaño fijo y en un principio lleno de relleno, el cual solo permite añadir elementos, minimizando el movimiento de la cabeza del disco, es decir, minimizando las operaciones de acceso aleatorio que suele ser la operación más lenta en un disco. 

Cuando un \textit{journal} está lleno se ocupa otro nuevo. A medida que se agregan registros de eliminación para los elementos de un \textit{journal}, HornetQ tiene un sofisticado algoritmo de recolección de basura, que puede determinar cuando es posible eliminar determinado \textit{journal}.

\section{Implementación}

El módulo de mensajería debe permitir la interacción, a nivel de ejecución, entre los módulos productores y consumidores con las colas que en él se hayan configurado. Con este fin, como parte del módulo se crearon diversas clases que funcionan como interfaces conectoras, las que permiten entre otras cosas la creación y el acceso remoto o local a estas colas. Estas interfaces serán utilizadas por el módulo de comunicación y el módulo consumidor de servicio para crear comunicarse entre ellos.

\subsection{Configuración del Módulo}

Este módulo pertenece a la capa aplicativa del \textit{framework} para la gestión de módulos. Siguiendo el modelo de configuración estándar definido, el módulo deberá ser configurado e implementado a través del archivo XML, como parte del conjunto de elementos que estarán presentes en el sistema (ejemplo de esta interfaz en el cuadro~\ref{lst:environment.xml} del apéndice A).

Particularmente podrán ser especificadas propiedades básicas como la dirección y el puerto del servidor que levantará las colas, además del directorio en donde se almacena físicamente la persistencia de los mensajes enviados.

Una vez configurado, el módulo levantará y monitoreará el funcionamiento de HornetQ en determinado servidor. La creación de colas y de sus consumidores y receptores estarán a cargo de los módulos de comunicación y servicios y no serán especificadas en la configuración del módulo de mensajería, sino que cada módulo podrá crear o acceder a determinadas colas de manera remota y dinámica en tiempo de ejecución .