\chapter{Arquitectura de la solución} \label{ch:arquitectura}

Las decisiones arquitectónicas tienen un efecto profundo y amplio en las etapas de desarrollo posteriores. La evaluación temprana de los requisitos y la arquitectura permite ahorrar tiempo y dinero, ya que la fijación de los defectos una vez que se envió el código es al menos tres veces más costoso \cite{McConnell01}.

Una arquitectura de \textit{software} es una entidad compleja que no se puede describir de una manera simple y unidimensional. Para hacerlo es posible utilizar lo que se conoce como vistas o perspectivas de arquitecturas que corresponden a representaciones del conjunto de elementos del sistema y sus relaciones  entre ellos \cite{Clements10}. Para documentar adecuadamente una arquitectura es posible identificar las vistas más importantes del sistema y añadir documentación que sea aplicable a más de una vista.

Estas vistas han sido frecuentemente observadas incluso cuando se han desarrollado sistemas totalmente distintos. Estas formas se conocen como estilos de arquitecturas.

Estos estilos de arquitecturas llevados al contexto de un problema particular permiten el establecer determinado enfoque de arquitectura. Cada estilo de arquitectura puede ser definido como la especialización de tipos de elementos y tipos de relaciones, además de restricciones de cómo ellos pueden ser usados \cite{kazman1996scenario}.

A diferencia de un patrón de arquitectura, el cual se enfoca en el problema y en el contexto, además de como resolver ese problema en ese contexto, un estilo de arquitectura se centra en el enfoque de la arquitectura, sin describir necesariamente cuando este estilo puede ser o no útil, por lo que su descripción resulta más abstracta que la de los patrones. 

La decisión sobre la arquitectura utilizada debe ser conducida por los objetivos y los atributos de calidad del sistema a diseñar \cite{Clements10}. El presente capítulo tiene como finalidad describir esos atributos de calidad, los estilos de arquitectura seleccionados para satisfacerlos y como son solucionados.

\section{Objetivos y atributos de calidad del sistema}

El objetivo de la plataforma es desarrollar un sistema de comunicación M2M compatible con dispositivos con Internet empotrada. Estos equipos pueden tener un protocolo de comunicación definido o contar con las herramientas para implementar uno, permitiéndoles transmitir y recibir información a través de Internet, pudiendo ser administrados de manera centralizada a través de una aplicación o sistema montado en una máquina de tipo servidor.

En el capítulo 2 se describieron los requerimientos para el desarrollo de un sistema con los objetivos mencionados, los que corresponden a escalabilidad, alta disponibilidad, portabilidad, extensibilidad y modularidad. Es posible solucionar todos estos atributos a través de consideraciones de arquitectura, pero también a nivel de la implementación de los elementos. Por ejemplo la portabilidad es posible solucionarla con consideraciones de arquitectura, como a través de un diseño de capas, pero también a nivel de las tecnologías de desarrollo de \textit{software} utilizadas, como por ejemplo a través de la utilización de Java como lenguaje de programación. Más detalles sobre portabilidad, este lenguaje de programación y más aspectos de implementación serán abarcados en los siguientes capítulos.

\section{Estilo de arquitectura cliente-servidor}

Tal como se describió en el capítulo 1, gracias a las nuevas posibilidades que entrega el Internet móvil la mayoría de los sistemas M2M implementan su sistema de comunicación basados en los protocolos TCP/IP, razón por la cual uno de los objetivos de esta memoria es el establecimiento de un sistema de comunicación compatible con ellos. Estos protocolos permiten implementar sistemas basados en el modelo de comunicación de tipo cliente-servidor o punto-a-punto (en inglés \textit{peer-to-peer}), pero la mayoría de los equipos M2M que se conectan a través de internet están diseñados como clientes que pueden formar parte de un sistema cliente-servidor.

La arquitectura cliente-servidor está compuesta principalmente por dos tipos de elementos:
\begin{itemize}
	\item Clientes, que son componentes que invocan servicios desde un servidor.
	\item Servidores, que son componentes centralizados que proporciona servicios a los clientes. Estos también pueden ser clientes de otros servidores.
\end{itemize}

Un servicio corresponde a una abstracción de un recurso computacional generalmente compartido por un servidor a través de una operación genérica o función que puede ser invocada por un cliente. El servicio es presentado a través de una interfaz simple que abstrae la complejidad subyacente, lo que permite a los usuarios acceder a servicios independientes sin el conocimiento de la aplicación de la plataforma de servicio.

Esta arquitectura utiliza para la comunicación entre sus elementos conectores de tipo petición/respuesta, los cuales implementan un protocolo del mismo tipo, permitiendo que un servidor pueda ofrecer más de un servicio. Además el flujo de comunicación es generalmente asimétrico, siendo los clientes quienes inician las interacciones invocando un servicio de un servidor.

La arquitectura cliente-servidor añade al sistema los atributos de escalabilidad y disponibilidad, ya que permite replicar clientes y/o servidores de manera independiente \cite{Clements10}. 

\subsection{Sistema de comunicación cliente-servidor}

Aplicado al contexto de un sistema M2M los dispositivos distribuidos a través de Internet corresponderán a los clientes de la arquitectura cliente-servidor, los cuales accederán a los servicios entregados por un sistema de comunicación, que corresponde al servidor, el cual se mantendrá esperando a que los clientes establezcan la comunicación y envíen datos. Suele implementarse de manera puramente síncrona, manteniendo una conexión abierta y a la espera de que la respuesta sea entregada o que el tiempo de espera expire. Sin embargo también es posible implementarla de forma asíncrona. 

Una representación de los elementos del estilo cliente-servidor puede apreciarse en la figura~\ref{fig:client-server}.

\begin{figure}[h]
   \centering
   \includegraphics[scale=0.7]{../images/client_server.eps}
   \caption{Elementos de la arquitectura cliente-servidor, el cliente envía una solicitud de datos hacia el servidor, el cual responde a la solicitud. Por lo general, hay una serie de intercambios antes de que el mensaje sea enviado completamente.}
   \label{fig:client-server}
\end{figure}

En la práctica serán los clientes quienes por lo general transmitan información hacia los servidores. Esto no va contra la definición del estilo cliente-servidor, ya que desde el punto de vista conceptual ellos interactúan con la abstracción de la información determinada por el servidor, a través de la definición de un protocolo.

El diseño y desarrollo del sistema de comunicación será abordado en el capítulo 6.

\section{Estilo de arquitectura orientada a servicios}

La arquitectura orientada a servicios (SOA) puede describirse como el estilo compuesto por dos tipos de elementos distribuidos, consumidor de servicios y el proveedor de servicios \cite{Clements10}. Los proveedores de servicio ofrecen un servicio que es invocado por los consumidores \cite{Bianco07}. 

Las restricciones entre consumidores y proveedores de servicios son:

\begin{itemize}
	\item Ambos elementos pueden ser implementados de manera independiente, perteneciendo a diferentes sistemas y/o organizaciones.
	\item Los consumidores servicios pueden enviar solicitudes para que los proveedores de servicios lo alimente con información (de manera directa o a través de un intermediario).
	\item Los proveedores de servicios pueden ser también consumidores.
\end{itemize}

Bajo esta arquitectura los servicios son unidades no asociadas de funcionalidades flexiblemente acopladas, que son autónomas. Al igual que en el estilo cliente-servidor, un servicio es representado por una interfaz simple que abstrae la complejidad subyacente.

Además de proveedores y consumidores de servicios la definición de SOA acepta la presencia de componentes especializados opcionales, específicamente de aplicaciones tipo ESB o \textit{middlewares} orientados a mensajes (ver capítulo 2), un servidor de orquestación y/o un servidor de registros para la ubicación de servicios.

La principal característica de la arquitectura orientada a servicio es el acoplamiento flexible (en inglés \textit{loose coupling}) entre proveedores y consumidores de servicio. Esto significa que cada uno de sus componentes tiene el mínimo conocimiento de las definiciones de otros componentes con los que interactúa. Esto favorece la interoperabilidad, ya que proveedores y consumidores pueden ser ejecutados en entornos diferentes, además de proveer al sistema atributos de modularidad y extensibilidad, al permitir añadir de manera simple nuevos servicios, y alta disponibilidad, ya que que el sistema puede funcionar relativamente normal ante la falla de uno de sus componentes.

El estilo SOA es muy similar a la arquitectura cliente-servidor, sin embargo está enfocado en el establecimiento de una infraestructura empresarial, y no simplemente en la forma en que dos elementos se comunican sobre una red. Su implementación tiene por objetivo reducir la brecha entre la estrategia y cambios en los procesos \cite{krafzig05}. Es por eso que puede incluir entre sus elementos aplicaciones de mensajería y presenta conectores adicionales a los determinados en el estilo cliente-servidor, enfocándose en el establecimiento de un sistema en donde la solución sea desarrollada por diferentes elementos distribuidos en ambientes distintos.

\subsection{Conectores usados en SOA}

De acuerdo a la definición de la arquitectura SOA la comunicación entre los elementos puede realizarse a través de dos tipos de tecnologías. Por una parte se pueden utilizar conectores de tipo llamada/retorno (en inglés \textit{call/return}), en los cuales el componente invocante del servicio pausa (o bloquea) el servicio hasta que este sea completado. Este tipo de conectores es propicio para protocolos de conexión de corta vida.

La otra posibilidad de comunicación es a través de mensajes asíncronos los cuales son almacenados de manera temporal por un sistema de mensajería cuando los módulos de destino están ocupados o no conectados.

Si hacemos un análisis comparativo entre ambos enfoques podemos concluir que el estilo llamada/retorno posee como principal ventaja la independencia de aplicaciones intermediarias, lo que permite ahorrar en recursos destinados a la gestión de los mensajes, permite mitigar la presencia de bloqueos mutuos y establecer una comunicación más rápida que el estilo asíncrono. Su principal desventaja es que ambos, proveedor y consumidor de servicios necesitan estar activos para permitir el envío de mensajes.

Por su parte el estilo basado en mensajes asíncronos tiene la ventaja que no requiere que el consumidor esté activo para el envío de mensajes, ya que estos pueden estar almacenados en el sistema de mensajería. Además esta misma propiedad permite la creación de clústers de manera limpia y sencilla, gracias a la posibilidad de agregar dinámicamente consumidores de servicios al sistema, permitiendo la distribución de la carga del procesamiento de la información recibida. La gran desventaja de este enfoque es la utilización de recursos extras para manejar la aplicación de mensajería. Además su utilización aumenta el riesgo de bloqueos mutuos en la comunicación.

\subsection{Sistema de comunicación orientado a servicios}

Aplicado al sistema de comunicación para dispositivos M2M, la arquitectura SOA tiene por objetivo desacoplar el procesamiento de los datos del manejo de la comunicación con los distintos dispositivos. Bajo este esquema se considerará como servicios a la canalización de determinados mensajes o eventos provenientes del conjunto de equipos conectados al sistema, permitiendo su procesamiento desacoplado y/o distribuido. Estos mensajes estarán constituidos por la información transmitida por el dispositivo, además de una serie de eventos referidos al ciclo de comunicación.

El proveedor de servicios corresponderá al mismo módulo de comunicación definido anteriormente, el cual por una parte podrá ser configurado con el protocolo de comunicación necesario para controlar los dispositivos M2M a través del modelo cliente-servidor, y por otra parte retransmitirá los mensajes recibidos hacia los consumidores de servicios. 

Los consumidores de servicios corresponden al siguiente elemento parte de la arquitectura del sistema a desarrollar, y su objetivo es procesar ciertos mensajes provenientes desde los dispositivos M2M, canalizados el módulo de comunicación, con el fin de procesar la información contenida de acuerdos a los requerimientos del sistema. 

Este esquema arquitectónico es una generalización del problema, ya que en la práctica la aplicación de red funciona también como consumidor de los servicios provenientes de las aplicaciones procesadoras, pero particularmente para permitir la comunicación hacia los dispositivos. Su principal labor corresponde a generar y canalizar eventos, y el de las aplicaciones procesadoras, consumirlos. 

Además la comunicación entre proveedores y consumidores será diseñada utilizando un modelo de comunicación asíncrona. Estos posibilitarán la implementación ágil de las diferentes necesidades que determinado proyecto pueda presentar, incorporando y extrayendo módulos de servicios sin la interrupción del funcionamiento general del sistema. Se prioriza esta propiedad por sobre los recursos necesarios para administrar este tipo de comunicación.

Una representación de los elementos del estilo SOA que serán utilizados en la arquitectura del sistema puede apreciarse en la figura~\ref{fig:soaele}.

\begin{figure}[h]
   \centering
   \includegraphics[scale=0.32]{../images/SOA.eps}
   \caption{Elementos de la arquitectura SOA utilizados en el sistema, proveedores y consumidores de servicios, además de una aplicación orientada a mensajes como mediador entre ambos.}
   \label{fig:soaele}
\end{figure}

\section{Arquitectura sistema de comunicación M2M}

Como ya se señaló, la arquitectura del sistema estará compuesta principalmente de los estilos de arquitecturas descritos, en base a los cuales se implementará un sistema compuesto por lo siguientes elementos:
\begin{itemize}
	\item Dispositivos M2M.
	\item Módulo de comunicación.
	\item Módulo de servicios.
	\item Módulo de mensajería.
\end{itemize}

El primero corresponde a la capa física distribuida del sistema y los tres últimos a aplicaciones de \textit{software} que pueden funcionar de manera independiente.

Uno de los objetivos de la memoria es permitir la implementación ágil del sistema de comunicación, lo que implica que los módulos pueden ser instanciados y configurados de manera sencilla por el usuario, preferiblemente en tiempo de ejecución de la aplicación. 

Esta implementación debe ser realizada a través de una interfaz estándar, lo que implica que los módulos deben estar desarrollados basados en una herramienta común que permita acceder a esa interfaz, desarrollar la lógica del módulo y gestionar su funcionamiento. Esta plataforma para el desarrollo y gestión de módulos es el siguiente elemento del sistema, y su diseño e implementación serán descritos en el siguiente capítulo.

En la figura~\ref{fig:soam2m} se aprecia un diagrama que representa la interacción entre los diferentes módulos que componen el sistema. Las flechas negras indican el flujo de información entre ellos y las punteadas blancas las acciones dinámicas como la inclusión de nuevos módulos consumidores de servicio y los diferentes servicios que serán provistos por el módulo de comunicación.

\begin{figure}[h]
   \centering
   \includegraphics[scale=0.32]{../images/SOA_M2M.eps}
   \caption{Representación de la interacción de los módulos que componen el sistema}
   \label{fig:soam2m}
\end{figure}

En el capítulo 2 de esta memoria se analizaron distintos aspectos que tienen relación con el desarrollo de una solución integral para el desarrollo de sistemas de comunicación M2M de manera genérica. En ese análisis también se nombraron distintos tipos de herramientas que solucionan estas problemáticas involucradas, cada una de las cuales es fruto de la investigación de algún prestigioso grupo de desarrolladores y por lo general distribuidas mediante el modelo de código abierto. No aprovechar esos esfuerzos y desarrollarlas de manera personalizada no tiene sentido en la época actual, por lo que la implementación de este proyecto estará compuesta por aquellas herramientas que más ventajas entregan al sistema. En los siguientes capítulos se describirán las piezas utilizadas en cada uno de los elementos del sistema.

