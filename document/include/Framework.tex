\chapter{Framework para el desarrollo y gestión de módulos} \label{ch:framework}

Como se describió en el capítulo anterior, la implementación del sistema se realizará utilizando diversas herramientas. La mayoría de estas herramientas pueden ser configuradas utilizando archivos XML, que funcionan como interfaz. Sin embargo, estas también pueden ser definidos en tiempo de compilación a través de la utilización directa de sus bibliotecas. Si bien la primera alternativa corresponde a un enfoque sencillo y fácil de implementar, carece de usabilidad cuando el sistema esta basado en herramientas distintas, ya que cada una deberá ser configurada de una manera diferente. El presente capítulo tiene por objetivo la descripción de la aplicación que permite la implementación estándar y el funcionamiento coordinado de las herramientas que componen el sistema.

\section{Gestión de módulos}
 
Existen aplicaciones conocidas como inyectores de dependencias las cuales permiten la creación de entornos modulares. Se caracterizan por una arquitectura basada en contenedores livianos que permiten ensamblar diferentes componentes de diferentes proyectos en una aplicación cohesiva. Ejemplos de este tipo de aplicaciones son Spring \cite{johnson2005introduction}, PicoContainter \cite{committers2008picocontainer} y JBoss \cite{jboss2011overview}.

Estas herramientas corresponden a completas plataformas de \textit{software}. Una plataforma (en inglés \textit{framework}) corresponde a un programa ocupado por desarrolladores de \textit{software} para implementar la estructura estándar de una aplicación. Su objetivo es abstraer al usuario de las capas de bajo nivel, que son necesarias para el funcionamiento de cualquier tipo de aplicaciones y que no necesitan ser reescritas una y otra vez. 

En los inyectores de dependencias las funciones de creación e inicialización de los objetos es separada del código que las utiliza. Esto trae consigo diversas ventajas como la creación de módulos más independientes que son más fáciles de probar mediante técnicas de pruebas unitarias (en inglés \textit{unit testing}) de forma aislada y que no requieren de cambios en la lógica de código ante una refactorización de las dependencias. Sin embargo la separación entre dependencias y código tiene una serie de desventajas, como por ejemplo puede provocar problemas al momento de diagnosticar y solucionar problemas. Además en la práctica la implementación del código está intrínsecamente relacionada con las dependencias que utilice. 

Por lo general las plataformas de inyección de dependencias funcionan en base a archivos de configuración detallados, lo que requiere que el desarrollador entienda el modelo de configuración, así como el código para cambiarlo.

\section{Implementación}

En base al análisis de las propiedades entregadas por un \textit{framework} inyector de dependencias se descarta su utilización como gestor de módulos, privilegiando el desarrollo de una aplicación para este objetivo.

Esta aplicación funcionará como base para todos los módulos desarrollados por el sistema, ya sea aquellos definidos como parte de la arquitectura como los que los controlarán de manera sincronizada.

La aplicación a desarrollar deberá funcionar por una parte como biblioteca proporcionando una serie de herramientas para el desarrollo de diferentes tipo de proyectos basados en arquitecturas modulares que utilizan otras bibliotecas de programación, y por otra como administrador a nivel de implementación, definiendo un modelo de configuración estándar y escalable que garantice la usabilidad del sistema.

\subsection{Lenguaje de programación}

Existen muchas aplicaciones relacionadas con el objetivo de esta memoria que están desarrolladas utilizando el lenguaje de programación Java \cite{gosling95}, cuyas características fueron descritas en el capítulo 2. Esto debido a la portabilidad que presentan las aplicaciones, gracias a la máquina virtual.

Desde el punto de vista del alto rendimiento, para el desarrollo de sistemas de comunicación posee ciertas cualidades provistas por la biblioteca Java NIO, la cual permite desarrollar sistemas de comunicación eficientes. Java es popularmente considerado como lento en comparación a lenguajes como C y C++, sin embargo muchos estudios demuestran lo contrario \cite{amedro2008current, lewis2003performance}.

En base a todas a las características que Java posee la plataforma será desarrollado utilizando este lenguaje de programación.

\subsection{Diseño basado en capas}

En base a las posibilidades que Java provee, la herramienta para la gestión de módulos será diseñada utilizando cuatro capas de abstracción, cada una de las cuales se encargará de los detalles de implementación de un conjunto particular de funcionalidades, ocultando su complejidad a capas superiores, lo cual garantiza y optimiza la reusabilidad del código.

\subsubsection{Capa de módulo con estados}

La primera capa de abstracción corresponde a la capa de módulo con estados y tiene por objetivo garantizar el alto grado de desacoplamiento entre los módulos y posibilitar la interacción entre ellos y su entorno. Está constituida por la clase abstracta de nombre {\tt StatefulModule}, que corresponde a la unidad modular independiente más básica de la aplicación, cuyo funcionamiento independiente será provisto mediante la utilización de una hebra (en inglés \textit{thread}) que permitirá su funcionamiento en paralelo a otras hebras dentro del sistema.

El funcionamiento de cada módulo estará determinado por un ciclo de vida de estados fijos, definido a través de la interfaz {\tt ModuleLifecycle}, que corresponde a un conjunto de métodos, cada uno de los cuales representa un estado particular y que pueden ser implementado por capas de abstracción superiores para añadir funcionalidades de acuerdo al estado actual. Cada una de estas funciones será ejecutada de forma iterativa por el propio módulo de acuerdo al ciclo de vida. Esta interfaz puede ser apreciada en la figura~\ref{lst:module_lifecycle}.

\begin{lstlisting}[language=Java,caption=Interfaz que implementa el ciclo de vida de los módulos del sistema, label={lst:module_lifecycle} ]
package m2m.stateful_module;

public interface ModuleLifecycle {
    public void onCreate() throws Exception;
    public void onConfigure() throws Exception;
    public void onStart() throws Exception;
    public void onStop() throws Exception;
    public void onRestart() throws Exception;
    public void onFailed() throws Exception;
    public void onDestroy() throws Exception;
}
\end{lstlisting}
%\caption{Interfaz ModuleLifecycle, la cual define los diferentes estados que componen el ciclo de vida de cada de los módulos que formarán el sistema.}
%\label{fig:module_lifecycle}

Para implementar adecuadamente la interacción entre el módulo y su entorno se desarrolló por una parte un sistema de notificaciones de eventos para comunicar estados internos del módulo hacia elementos externos, y por otra parte funciones especializadas para controlar su funcionamiento desde el exterior. El sistema de notificaciones esta basado en el patrón de diseño observador \cite{gamma1994design}, el cual permite informar la transición de estados que realice el módulo a cualquier elemento del sistema que implemente la interfaz {\tt ModuleListener} y que se registre debidamente.

Cada hebra ejecutará iterativamente las funcionalidades descritas; primero verificará la existencia de algún evento pendiente definido de manera externa, luego ejecutar la función del estado actual del módulo y finalmente notificará de su ejecución a los interesados. Este funcionamiento iterativo se desarrolló basado en la recomendación hecha por la compañía Sun sobre la deprecación del método stop de la clase Thread \cite{Oracle}. La figura~\ref{fig:statefulseq} representa este funcionamiento.

\begin{figure}[h]
   \centering
   \includegraphics[scale=0.32]{../images/stateful_seq.eps}
   \caption{Diagrama de secuencia de la clase Stateful Module}
   \label{fig:statefulseq}
\end{figure}

%\subsection{Sistema de registro para módulos independientes}

Uno de los problemas que emergen durante el proceso de desarrollo de un orquestador de un entorno modular es el diseño del sistema de registros (o en inglés \textit{log}) de los eventos que ocurren durante el funcionamiento de cada uno de ellos, el cual es fundamental para comprender la actividad del sistema y para diagnosticar problemas.

Si bien Java provee de una biblioteca para realizar esta función (denominada Java Logging API), existen diversas herramientas distribuidas a través de código abierto que poseen más y mejores características que garantizan la implementación adecuada de sistemas de registros particularmente para Java, entre las que destacan Log4J \cite{gulcu2002short} y LogBack \cite{logback}.

El principal problema de registrar las actividades en entornos modulares es que las técnicas de archivos de registros en Java utilizan un enfoque estático para aumentar su rendimiento, lo cual posibilita la definición de un archivo de registro por una determinada clase, y no uno por cada instancia. Esto provocaría que cada usuario que quiera analizar el funcionamiento del determinado módulo debería acceder a un archivo, el cual contiene la información de todos los módulos del sistema. La herramienta LogBack permite solucionar este problema a través de la definición de registros por instancia mediante la utilización directa de su biblioteca. 

Uno de los principales problemas de los sistemas de registro es su costo computacional. Esta es una preocupación legítima ya que incluso las aplicaciones de tamaño moderado puede generar archivos de cientos de \textit{megabyte} (MB) de tamaño en un día de funcionamiento. LogBack cuenta con una ventaja considerable frente a otras soluciones en cuanto a rendimiento gracias al diseño de su arquitectura, la cual permite el desactivar niveles de registro de manera automática para aumentar el rendimiento de la aplicación, propiedad no presente en bibliotecas alternativas.

Por estas razones LogBack será la herramienta utilizada para posibilitar la definición de registros por instancias de módulos, incluyendo su administración en la capa {\tt StatefulModule}.

%\subsection{Constructores de módulos}

Para la creación y construcción de módulos se utilizó el patrón de diseño de \textit{software} constructor \cite{gamma1994design} (en inglés \textit{builder}), cuyo objetivo es solucionar el problema de creación de objetos cuando estos poseen un gran número de variables que deben ser definidas para su funcionamiento. Para evitar este problema, conocido como el antipatrón constructor telescópico (en inglés \textit{telescoping constructor}), se utiliza un objeto especializado denominado constructor, que recibe los parámetros necesarios uno por uno, para luego retornar el objeto construido.

El diseño de la herramienta de gestión de módulos plantea la existencia de un constructor por cada definición de algún módulo, con el fin de permitir una correcta reutilización del código. De esta manera cada una de las distintas implementaciones que hereden esta clase podrán crear una implementación personalizada de un constructor que hereden a la clase constructor. De esta manera {\tt StatefulModule}, el módulo más básico del sistema, posee su propio constructor, {\tt StatefulModuleBuilder} que permite su creación. Cada módulo que extienda la clase {\tt StatefulModule}, deberá contar con un constructor que herede a {\tt StatefulModuleBuilder}. El diagrama de clases de la figura~\ref{fig:absbuildersarch} representa este diseño.

\begin{figure}[h]
   \centering
   \includegraphics[scale=0.32]{../images/abs_builders_arch.eps}
   \caption{Diagrama de clases que representa el patrón de diseño constructor, utilizado para la creación de los módulos de la plataforma}
   \label{fig:absbuildersarch}
\end{figure}

\subsubsection{Capa de módulo autoconfigurable}

A lo largo del proyecto nos encontramos con varios módulos que dependen de determinados archivos, ya sea para configuración o para definir aspectos de su funcionamiento. En vista de aquellos se diseñó el módulo {\tt AutobuildableModule} que tiene la característica de guardar la instancia de su constructor y monitorear constantemente un grupo de archivos asociados a él, permitiendo identificar sus modificaciones para volver a configurar el constructor guardado y reconstruir la instancia del módulo.

El constructor que utiliza este tipo de módulos es una extensión de {\tt StatefulModuleBuilder} llamada {\tt AutomaticModuleBuilder}. Este constructor posee el comportamiento adecuado para permitir su reconfiguración y la reconstrucción de determinado módulo.

\begin{figure}[h]
   \centering
   \includegraphics[scale=0.62]{../images/autobuildable_module.eps}
   \caption{Diagrama que representa el funcionamiento del módulo {\tt AutobuildableModule}. El módulo guarda una instancia de su constructor, el cual además monitorea un conjunto de archivos, reconstruyendo el módulo ante cambios.}
   \label{fig:absbuildersarch}
\end{figure}

\subsubsection{Capa de entorno modular}

Hasta este punto la plataforma para el desarrollo y gestión de módulos posee una capa que define módulos y otra que permite reiniciar estos módulos ante la modificación de algún archivo en particular. La siguiente funcionalidad corresponde a la administración de módulos la cual será desarrollada en la capa denominada {\tt Modular Environment}, compuesta principalmente por la clase administradora {\tt EnvironmentManager}, la cual tiene por objetivo la definición, creación y administración coordinada de los diferentes módulos que componen este entorno. Su implementación corresponde a un módulo especializado que a través de su ciclo de vida se encargará de administrar el funcionamiento de módulos que hereden la clase abstracta {\tt EnvironmentModule}.

{\tt EnvironmentManager} corresponde a una implementación particular de un módulo del sistema, y al igual que todos ellos deberá ser construida utilizando un constructor. Particularmente su constructor corresponde a la clase {\tt EnvironmentManagerXMLBuilder}, la que determina modelo de configuración único y escalable, definidos mediante un archivo de tipo XML \cite{bray1998extensible}, el cual determina los módulos que componen ese entorno particular, además de definir distintas propiedades configurables del {\tt EnvironmentManager}.

Además esta capa hereda a la capa {\tt AutomaticModuleBuilder}, permitiendo reiniciar el ciclo de vida de {\tt EnvironmentModule} de acuerdo a las modificaciones que se realicen al archivo de configuración, lo cual es útil para reconstruir o añadir módulos al entorno a través esta interfaz. Además dentro de este archivo pueden ser definidas ciertas reglas de coordinación para el funcionamiento de los módulos, permitiendo que estos pasen a determinados estados dependiendo del estado de otros módulos del mismo entorno local.

\subsubsection{Capa aplicativa}

La capa aplicativa corresponde a todo aquellos módulos que formarán parte del sistema y que serán administrados por la capa de entornos modular. Técnicamente se refiere a todas las implementaciones de la clase {\tt EnvironmentModule}, cada una de las cuales funcionará como base para el desarrollo de los módulos parte del sistema, los que serán implementados a nivel de código utilizando bibliotecas específicas y configurados a través de un archivo XML (ver figura~\ref{fig:framework_modules}).

\begin{figure}[h]
   \centering
   \includegraphics[scale=0.6]{../images/framework_modules.eps}
   \caption{Diagrama que representa la utilización de la plataforma para el desarrollo modular para implementar los elementos del sistema.}
   \label{fig:framework_modules}
\end{figure}

El diseño, desarrollo e implementación de los módulos que pertenecen a esta capa serán descritos en los siguientes capítulos.

\begin{figure}[h]
   \centering
   \includegraphics[scale=0.42]{../images/framework_class_diagram.png}
   \caption{ Diagrama de clases del framework de desarrollo de entornos modulares. Las capas {\tt AutoconfigurableModule} y {\tt StatefulModule} corresponde al framework de desarrollo. {\tt ModularEnvironment} corresponde a la capa orquestadora de módulos. La capa {\tt UserDevelopment} es aquella donde el usuario desarrolla los distintos módulos que formarán parte de su sistema}
   \label{fig:frameworkarch}
\end{figure}

La figura~\ref{fig:frameworkarch} contiene una representación del diagrama de clases de la aplicación para la gestión de módulos, el cual se encuentra dividido de acuerdo a las diferentes capas descritas en esta sección.  
