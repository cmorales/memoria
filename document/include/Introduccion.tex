\chapter{Introducción}
\label{ch:introduccion}
La era actual está caracterizada por el auge de la tecnología aplicada a los sistemas de comunicación. La sociedad ha cambiado sus costumbres y modo de vida, primero por la aparición de la tecnología celular, permitiendo la comunicación por voz de manera inalámbrica y luego por el apogeo de Internet, que trajo consigo, no hace mucho tiempo, el auge de los teléfonos inteligentes y de Internet móvil. 

Las tecnologías relacionadas con estas redes móviles proporcionan actualmente una cobertura y movilidad casi total en todo el país y están preparados para ofrecer un servicio de datos de banda ancha a un costo significativamente menor que en el pasado, gracias a una normalización general del mercado \cite{Meeker13, Wu11}. Estos factores ofrecen condiciones únicas para el nacimiento de nuevos tipos de aplicaciones y servicios, que en su conjunto brindan una conectividad casi ilimitada, entregando un mundo de información a las personas.

\section{Internet de las cosas}

En la actualidad no solo de personas esta constituido Internet, convivimos día a día con una serie de dispositivos y sistemas que conforman lo que se conoce como Internet de las cosas (o en inglés \textit{Internet of Things}, IoT), un paradigma cuyo concepto fue acuñado por primera vez en 1999 por Kevin Ashton \cite{ashton2009internet}, cofundador del centro Auto-ID del Massachusetts Institute of Technology (MIT):

\begin{quote}
\textit{Si tuviéramos equipos que sepan todo lo que hay que saber sobre las cosas (a partir de datos que obtuvieron sin la ayuda de nosotros) podríamos ser capaces de rastrear y contar todo, y reducir considerablemente pérdidas y costos.}
\vskip0.8mm
\hspace*\fill{\small--- Kevin Ashton, Massachusetts Institute of Technology (MIT)}
\end{quote}

Este concepto considera que miles de millones de objetos y su entorno circundante estarán conectados y controlados por un rango de equipos, redes de comunicaciones y servidores, permitiendo su interacción autónoma para alcanzar objetivos comunes \cite{Wu11, vermesan13}.

Esta idea no es solo una teoría, según un estudio realizado por Cisco Internet Business Solutions Group (IBSG), se estima que entre el 2008 y el 2009 el número de equipos conectados superó al de personas en el mundo (ver figura~\ref{fig:people_vs_machine}), momento que es considerado como el nacimiento de Internet de las cosas \cite{evans2011internet}.

\begin{figure}[!h]
   \centering
  	\includegraphics[width=\linewidth]{../images/people_vs_machine.png}
   \caption{Cuadro comparativo entre los equipos conectados a Internet y la población mundial. Fuente Cisco IBSG.}
   \label{fig:people_vs_machine}
\end{figure}

La aparición y proliferación de Internet de las cosas ha fomentado considerablemente la creación de nuevos y variados tipos de aplicaciones y servicios (ver cuadro~\ref{table:m2mapp}), los que están conformados por un amplio rango de dispositivos distribuidos que interactúan de forma cooperativa y/o controladas de manera centralizada por algún tipo de aplicación, creando un mundo de entornos inteligentes en áreas como la energía, el transporte y las ciudades en general \cite{Wu11}. 
	
\begin{table}
\small
\begin{tabular}{|>{\columncolor[gray]{0.9}}l|m{9cm}|}
\hline
Seguridad y seguridad ciudadana & Sistema de vigilancia, control de acceso físico (ej. edificios), monitoreo ambiental (ej. para desastres naturales), copia de seguridad para los teléfonos fijos. \\ \hline
Red eléctrica inteligente       & Electricidad, gas, agua, calefacción, control de red, medición industrial, respuesta en demanda.\\ \hline
Seguimiento y rastreo           & Gestión de pedidos, seguimiento de activos, supervisión humana. \\ \hline
Telemática vehicular            & Gestión de flotas, seguridad de auto/conductor, navegación mejorada, información de tráfico, peajes, pago durante conducción, diagnóstico remoto de vehículos.\\ \hline
Pago                            & Puntos de ventas, cajeros automáticos, máquinas expendedoras, máquinas de juegos.\\ \hline
Cuidado de la salud             & Monitoreo de signos vitales, apoyo a ancianos y discapacitados, telemedicina, diagnóstico remoto\\ \hline
Mantenimiento remoto y control  & Información industrial, sensores, iluminación, bombas, control de máquinas expendedoras. \\ \hline
Equipos consumidores            & Marco de fotos digital, cámaras digitales, ebooks, controladores de hogar y domótica.\\ \hline
\end{tabular}
\caption{Cuadro con los tipos de aplicaciones categorizadas dentro de Internet de las cosas (Fuente \cite{Wu11}).}
\label{table:m2mapp}
\end{table}

\subsection{M2M}

Parte integral de Internet de las cosas son las tecnologías involucradas, denominadas comúnmente como máquina a máquina (o en inglés \textit{machine to machine}, M2M), las cuales consisten en las herramientas que permiten comunicación inalámbrica o alámbrica entre equipos comúnmente denominados dispositivos M2M. 

Las arquitecturas de los sistemas de M2M clásicos están compuesta por pequeños dispositivos conectados de manera local con equipos enrutadores, los cuales funcionan como mediadores entre estos y servidores de comunicación. Bajo este esquema los dispositivos realizan labores relacionadas con la medición de variables y los enrutadores se encargan del procesamiento parcial de esta, su retransmisión hacia un servidor y el control coordinado del grupo de dispositivos adyacentes, esto con el objetivo de minimizar las responsabilidades de los dispositivos M2M debido a lo limitado de sus recursos de cómputo.

La ley de Moore \cite{moore1965cramming} ha descrito con gran exactitud el crecimiento acelerado de la cantidad de transistores en los circuitos integrados, lo que está directamente vinculado con las capacidades de los dispositivos electrónicos en general, permitiendo que en la actualidad puedan realizar más operaciones que en el pasado a un menor precio. Por otro lado, gracias a la reducción sostenida del costo de acceso, Internet móvil se ha transformado en la manera más rentable de controlar dispositivos de manera remota. Bajo este contexto ha nacido una nueva generación de dispositivos, los que poseen Internet empotrada (en inglés \textit{embedded Internet}) que les permite comunicarse de manera inalámbrica a sistemas de comunicación centralizados, sin la necesidad de intermediarios \cite{intelembint, Wu11}. Este nuevo tipo de dispositivos ha simplificando aún más las arquitecturas de comunicación de los sistemas M2M, siendo común hoy en día que el término se aplique a arquitecturas en donde la conexión entre dispositivos y un sistema central se realiza de manera directa. En la figura~\ref{fig:m2m_arch_types} se puede apreciar un diagrama de comunicación con las arquitecturas M2M clásica y moderna.

\begin{figure}[!h]
   \centering
  	\includegraphics[width=\linewidth]{../images/m2m_arch_types.eps}
   \caption{Diagrama simplificado que representa las arquitecturas clásica y moderna de los sistemas M2M.}
   \label{fig:m2m_arch_types}
\end{figure}

El mercado M2M está en constante crecimiento y se espera que un gran número de dispositivos sean integrados durante los próximos años, por lo que la escalabilidad de Internet es considerada un factor crítico. Por otro lado, si bien el procesamiento distribuido es fundamental para hacer frente a la complejidad de las aplicaciones M2M, la toma de decisiones centralizada y la gestión de miles de millones de dispositivos dentro de la nube se convertirán en un valor esencial de Internet de las cosas \cite{Wu11}.

\subsection{Desarrollo de aplicaciones M2M}

Desde el año 2012 a la fecha, el autor ha participado en el diseño y desarrollo de diversos sistemas de comunicación para aplicaciones M2M, entre las que destacan plataformas para la gestión de flotas de automóviles, supervisión de personal, monitoreo de variables ambientales de plantas de invernadero y el monitoreo remoto de motores de camiones. 

En base a esa experiencia fue posible determinar que, desde el punto de vista del \textit{hardware}, existen variadas plataformas denominadas de  \textit{hardware} abierto, basadas en placas con microcontroladores \cite{upton2013raspberry, kushner2011making}, que permiten facilitar el uso de la electrónica en proyecto multidisciplinarios. Por lo tanto, ante la ausencia en el mercado de dispositivos que cuenten con ciertas propiedades para implementar algún tipo de aplicación M2M particular, es posible utilizar estas alternativas para implementar dispositivos con sensores o artefactos personalizados, que incluyan sistemas con Internet empotrada \cite{doukas2012building}.

El auge de estos tipos de equipos ha permitido que hoy en día la mayoría de los sistemas M2M utilicen alguno de los protocolos que forman parte del conjunto de protocolos TCP/IP \cite{postel1981internet,leiner1985darpa} (protocolos de la familia de Internet), por lo que para su control distribuido es suficiente desarrollar un sistema de comunicación basado en la arquitectura cliente-servidor, modelo utilizado ampliamente por las aplicaciones que se relacionan de forma cooperativa sobre Internet, en el que el servidor provee de una función o servicio a uno o muchos clientes que lo solicitan \cite{Clements10}. Estos dispositivos incluyen bibliotecas de programación para desarrollar la lógica de comunicación y las rutinas de obtención de datos, o poseen un protocolo de comunicación definido y detalladamente documentado.

Desde el punto de vista de \textit{software} se comprobó que, debido al auge de los dispositivos con Internet empotrada, los sistemas de comunicación M2M están compuestos principalmente por aplicaciones centralizadas, alojadas en máquinas servidores de gran capacidad, diseñadas en base a la arquitectura cliente-servidor y diferenciadas por elementos particulares que dependen del servicio o producto, como lo son la definición del protocolo del dispositivo del sistema y de las rutinas de procesamiento de datos que el producto requiere, entre otros.

Además, frecuentemente en etapas tempranas de los proyectos se omiten consideraciones de arquitectura para las aplicaciones que corren en los servidores de control, que garanticen la escalabilidad técnica del producto/servicio, principalmente debido a problemas de presupuesto, mal diseño de \textit{software} o mala estimación de los tiempos comprometidos. Por ejemplo, es común que las soluciones desarrolladas mezclen el procesamiento de datos con la lógica de comunicación, lo cual impide añadir nuevas funcionalidades sin modificaciones a nivel de código, o representa un gran riesgo para el sistema completo, ya que un error en un módulo de procesamiento de datos puede significar la caída de todo el sistema. 

Se consideran como requerimientos de diseño básicos para un sistema de estas características las siguientes propiedades \cite{krafzig05}:
\begin{itemize}
	\item \textbf{Escalabilidad}, capacidad de adaptarse y reaccionar frente al crecimiento continuo de trabajo de manera fluida, sin perder calidad. El sistema debe para permitir múltiples conexiones con dispositivos y múltiples rutinas de procesamiento de información.
	\item \textbf{Alta disponibilidad}, propiedad que asegura cierto grado de continuidad operacional del sistema y de los servicios entregados.
	\item \textbf{Portabilidad}, característica que posee un aplicación para ejecutarse en diferentes plataformas. El sistema debe adaptarse de manera simple a nuevos entornos de trabajo, sin alterar su funcionamiento normal.
	\item \textbf{Extensibilidad}, que se refiere a la capacidad de un sistema para extender sus funcionalidades, en la que la estructura interna del sistema y flujo de datos están mínimamente o no afectados. Característica fundamental en las tecnologías M2M \cite{Wu11}.
	\item \textbf{Modularidad}, se refiere a las consideraciones de diseño que permiten que un \textit{software} complejo pueda ser manejable para facilitar la implementación y el mantenimiento. 
\end{itemize}

\section{Planteamiento del problema}

Actualmente no existen plataformas, distribuidas a través de código abierto, que permitan implementar de manera íntegra y ágil un sistema de comunicación para la gestión centralizada de dispositivos con Internet empotrada, en donde la definición del protocolo de comunicación y de los servicios de procesamiento de datos en la aplicación servidor sean altamente extensibles, y que cumpla con los requerimientos de diseño necesarios para este tipo de aplicaciones, lo cual es posible utilizando la tecnología existente.

Si bien existen plataformas de código abierto que permiten el desarrollo de sistemas de este tipo, están enfocadas al control de dispositivos compatibles con estándares acotados, o presentan compatibilidad con limitados protocolos de comunicación, generalmente HTTP \cite{postel1981internet}, careciendo de la extensibilidad necesaria \cite{devicehive}. Otras están diseñadas para arquitecturas que consideran necesariamente la presencia de un enrutador que se encarga de controlar dispositivos a su alrededor \cite{om2m}.

Existen empresas que ofrecen el servicio ofreciendo compatibilidad con protocolos de manera extensible, pero éstas utilizan herramientas privativas que no están disponibles para el desarrollo abierto, y la información recopilada por los dispositivos es almacenada en sus servidores, lo cual limita su procesamiento \cite{carriots,iobridge,sitewhere}. 

Otro grupo de aplicaciones distribuidas a través de código abierto solucionan parcialmente los requerimientos mencionados para este tipo sistemas, aunque están ampliamente masificadas y validadas. Por ejemplo existen diversas plataformas para los lenguajes de programación C++ \cite{dawes2009boost} y Java \cite{apache2009apache,netty2014,grizzly2014} que permiten el desarrollo rápido y fácil de aplicaciones de red tales como servidores y clientes basados en protocolos de la familia TCP/IP. Sin embargo, todas estas permiten la definición del protocolo y de las características del tipo de comunicación a nivel de código en tiempo de compilación, careciendo de la extensibilidad y usabilidad necesaria para el soporte de nuevas tecnologías y diferentes rutinas de procesamiento de información. Además el objetivo de estas bibliotecas es el manejo de la comunicación, por lo que para implementar de manera íntegra una aplicación de tipo M2M se debe desarrollar sobre ella la lógica de comunicación y el servicio particular determinado por la finalidad del tipo de sistema M2M desarrollado.

\section{Solución propuesta y objetivos}

Para dar una solución a la problemática identificada se propone desarrollar una plataforma que permita la implementación ágil, íntegra y extensible de un sistema de comunicación M2M compatible con dispositivos con Internet empotrada, en donde la gestión centralizada de estos dispositivos sea posible gracias a la configuración de una aplicación de tipo servidor alojada en una máquina destinada al alto rendimiento.

Esta configuración podrá realizarse en tiempo de ejecución, y permitirá definir el protocolo de comunicación soportado por el dispositivo, y las rutinas de procesamiento de datos determinadas por el objetivo del sistema a desarrollar.

Además será diseñado en base a la arquitectura orientada a servicios, proporcionando una interfaz abstracta a través de la cual servicios dedicados al procesamiento de datos pueden interactuar, desacoplándolos del manejo de la comunicación. Esto brindará al sistema características como alta disponibilidad, extensibilidad y modularidad, posibilitando la implementación de diferentes tipos de configuraciones de red en los servidores en donde sea implementado, incluyendo las de tipo clústers de computadores.

Para lograr esto el autor propone llevar a cabo los siguientes objetivos:

\begin{itemize}
	\item Investigar y diseñar la arquitectura de sistema, basada en el estilo de arquitectura orientada a servicios, que permita la integración modularizada, independiente y escalable de los módulos de comunicación y de procesamiento de datos.
	\item Diseñar y desarrollar una aplicación que permita el desarrollo, implementación y gestión modularizada de los elementos que conformen el sistema.
	\item Desarrollar un módulo de comunicación escalable, de alto desempeño y extensible, que sea compatible con los protocolos de la familia TCP/IP, particularmente protocolos TCP.
	\item Desarrollar un módulo que permita el análisis de los datos provenientes de los dispositivos y la definición ágil de las rutinas de procesamiento que conlleva el análisis.
	\item Validar el desarrollo del sistema mediante pruebas de implementación.
\end{itemize}

\section{Estructura del documento}

El capítulo 2 de esta memoria corresponde al estado del arte de las diferentes tecnologías involucradas en el desarrollo sistemas de tipo M2M, comenzando con una reseña histórica de este tipo sistemas, seguido de una descripción de las últimas tecnologías, estándares, y protocolos involucrados en el diseño, desarrollo e implementación de sistemas de comunicación de alto rendimiento.

El siguiente capítulo describe las arquitecturas más importantes presentes en la plataforma diseñada, principalmente la arquitectura orientada a servicio. La arquitectura del sistema definirá sus componentes, cada uno de los cuales será desarrollado y gestionado por una plataforma común, la cual será descrita en el capítulo 4. 

Los componentes manejados por la plataforma para el desarrollo y gestión de módulos corresponden a tres tipos de módulos, un módulo de comunicación, módulo de mensajería y módulo de servicio, cada uno de los cuales es descrito en los capítulos 5, 6 y 7, respectivamente.

Finalmente el capítulo 8 contiene las conclusiones que el autor rescató de las distintas etapas que formaron parte del desarrollo del proyecto, incluyendo la elaboración de esta memoria.

Además, a modo de Anexo, se incluye un manual de uso con una descripción detallada del desarrollo de una aplicación básica.