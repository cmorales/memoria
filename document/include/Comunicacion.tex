\chapter{Módulo de comunicación} \label{ch:comunicacion}

El módulo de comunicación corresponde al elemento del sistema que integra dos arquitecturas distintas. Por una parte funciona como servidor, al controlar los dispositivos M2M del sistema utilizando la arquitectura cliente-servidor, y por otra como proveedor de servicios bajo la arquitectura orientada a servicios. El servicio que entregará corresponderá a la canalización de los mensajes provenientes de los equipos conectados al sistema y de los eventos relacionados con la comunicación, permitiendo su procesamiento desacoplado y/o distribuido a través del módulo consumidor de servicios.

El presente capítulo tiene por objetivo explicar el diseño, desarrollo e implementación de este módulo.

\section{Modelo de implementación}

Una de las características más importante del sistema es la compatibilidad que debe existir entre el módulo de comunicación y los diferentes protocolos de comunicación que los dispositivos M2M pueden soportar. Este conjunto está acotado a la familia de protocolos de Internet, tal como fue definido en el capítulo 1.

Además debe ser capaz de canalizar ciertos eventos relacionados con la comunicación con los dispositivos M2M, lo cual será especificado por el usuario a través de la configuración en tiempo de ejecución.

Para permitir estas propiedades se definió un modelo de implementación del módulo basado en dos interfaces, una que permitirá al usuario configurar el protocolo de comunicación del tipo de dispositivo, integrando al sistema una serie de eventos, y otra que determinará cuales de ellos deben ser distribuidos a ciertos módulos.

\subsection{Protocolos de comunicación}

El protocolo de comunicación corresponde al conjunto de normas compartidas entre dos equipos electrónicos para permitir la comunicación entre ellos y el flujo de información, aún cuando estos pueden funcionar en base a tecnologías distintas. En los dispositivos M2M generalmente estas normas están definidas a través de mensajes de comunicación compuesto por una serie de parámetros. 

En base a la experiencia del autor se sabe que es posible que algunos dispositivos ocupen distintos tipos de mensajes, con distintos tipos de codificaciones, debido a que la comunicación se basa en la comunión de diversas capas en los dispositivos, algunas de ellas enviando mensajes totalmente distintos. Por esta razón, para su adecuado procesamiento, es necesario definir tanto decodificadores de mensajes, como sus analizadores. Ambos elementos corresponden a rutinas de código que pueden ser implementadas a través de funciones simples. Por una parte los decodificadores son funciones que toman por parámetro el mensaje recibido, para luego decodificarlo y retornarlo transformado. Los analizadores son elementos que se encargan de dividir los diferentes parámetros presentes el mensaje, por lo que tienen como argumento el mensaje decodificado y entregan un arreglo con los parámetros presentes en él.

Para definir el protocolo de comunicación es necesario especificar todos los elementos descritos. Esto es una tarea relativamente sencilla utilizando alguna de las herramientas de red que se describieron en el capítulo 2, sin embargo es solo posible de una manera en que la definición del módulo de comunicación se hace dependiente de ellas, en tiempo de compilación.

\subsection{Interfaz del dispositivo}

Con el objetivo de independizar el módulo de la definición del protocolo de comunicación con los dispositivos, se definió como método de definición de una interfaz de dispositivo a un archivo de configuración de formato XML que, en base a un conjunto de elementos y atributos XML, define los mensajes que el dispositivo envía para comunicarse, denominados tramas. Cada uno de estas tramas corresponde a un mensaje del protocolo, definido a través de un identificador, un decodificador, un analizador y un conjunto de parámetros. A su vez cada parámetro estará definido en la interfaz por un identificador y un nombre descriptivo. La definición de la interfaz también permite señalar algún parámetro que corresponda a un identificador único del equipo controlado, para proveer de información adecuada a la hora de depurar el funcionamiento del sistema.

Como se mencionó, tanto decodificadores como analizadores corresponden a elementos del sistema que pueden ser definidos a través de funciones simples con parámetros y retornos particulares. Para lograr la independencia del sistema con la definición del código particular de esas funciones se desarrolló un completo sistema de compilación e integración de clases, basado la biblioteca Java Reflections \cite{Forman04}, la cual es utilizada por los programas que requieren la capacidad de examinar o modificar el comportamiento en tiempo de ejecución de aplicaciones que se ejecutan en la máquina virtual Java. Este sistema permite definir las clases Decoders y Parsers y señalar su ubicación dentro del sistema de archivos para ser compiladas e integradas en tiempo de ejecución.

En el apéndice A, la figura~\ref{lst:device_interface.xml} corresponde a una interfaz de dispositivo para un ejemplo.

\subsection{Interfaz de servicios}

El módulo de comunicación es el elemento del sistema que provee de servicios. Como ya se mencionó, estos servicios corresponden a la canalización de determinados mensajes o eventos del conjunto de equipos conectados al sistema, permitiendo su procesamiento desacoplado y/o distribuido. 

Al igual que la definición de los mensajes del protocolo, los servicios serán implementados independientes de la definición del módulo, utilizando un archivo XML que permita definir una interfaz de servicio, que corresponde a la especificación de los distintos mensajes y/o eventos canalizados a determinadas colas del módulo de mensajería. Estas colas de mensajería corresponden a los servicios a los cuales los consumidores de servicios pueden conectarse (ver capítulo 5 y 6).

Dentro de esta interfaz es posible definir una serie de eventos a través de los siguientes elementos:

\begin{itemize}
	\item {\tt OnInboundFrameEvent}, que corresponde al evento relacionado con la llegada de un mensaje desde el dispositivo.
	\item {\tt OnDeviceConnectedEvent}, que corresponde al evento que se gatilla al conectarse un nuevo dispositivo.
	\item {\tt OnDeviceDisconnectedEvent}, que corresponde al evento que se produce cuando un equipo se desconecta.
	\item {\tt OnDeviceIdentifiedEvent}, que corresponde al evento que se produce cuando el equipo es identificado, posiblemente al extraer su parámetro identificador de algún mensaje.
	\item {\tt OnOutboundFrameEvent}, que corresponde al evento gatillado desde el servicio cuando este intenta enviar un mensaje al equipo. Este elemento no puede ser repetido.
\end{itemize}

Dentro de cada uno de estos eventos puede ser agregado otros elementos. Por ejemplo pueden contener algún mensaje de servicio, representado por el elemento {\tt ServiceMessage}. Este a su vez debe tener asociado un elemento {\tt InboundQueue}, que corresponde a la cola de comunicación a utilizar en el módulo de mensajería para comunicarse con determinado módulo de servicio. Es posible además definir mensajes de respuesta fijos representados por elemento {\tt FixedResponse} dentro de cada evento que lo soporte, lo que permitirá implementar fácilmente mensajes de tipo acknowledgement. 

En el apéndice A, la figura~\ref{lst:events_interface.xml} corresponde a una interfaz de dispositivo para un ejemplo.

\section{Desarrollo}

El módulo de comunicación fue desarrollado en base al \textit{framework} de comunicación Netty \cite{netty2014}, el cual permite construir aplicaciones basadas en la arquitectura cliente/servidor, brindando escalabilidad y alto desempeño, entre otras características.

Netty basa su diseño en el paradigma orientado a eventos, el cual determina el ciclo de funcionamiento de la aplicación, facilitando el desarrollo y la mantención de las aplicaciones de comunicación. Al igual que variadas herramientas basa su funcionamiento en la interfaz Java NIO \cite{hitchens02}, abstrayendo al usuario de la complejidad de los métodos utilizados para su manejo (ver capítulo 2).

La unidad más básica de Netty corresponde a los controladores de canales, que corresponden a clases especializadas en la intercepción y el manejo de diferentes eventos de un determinado canal de comunicación. Es posible definir listas ordenadas de controladores de canales llamadas tuberías de comunicación, las cuales interceptan los diferentes eventos de comunicación y los propaga a las otras capas en base a un determinado orden, permitiendo su configuración dinámica. Este patrón de diseño se conoce como filtro de intercepción \cite{crawford2003j2ee} y se encuentra representado en la figura~\ref{fig:nettypipeline}.

\begin{figure}[h]
   \centering
   \includegraphics[scale=0.32]{../images/netty_pipeline.eps}
   \caption{Diagrama que describe como los eventos I/O son procesados por las clase {\tt Channel Handlers} en un {\tt Channel Pipeline}}
   \label{fig:nettypipeline}
\end{figure}

Cada uno de los controladores de comunicación puede pertenecer a un sentido de la comunicación, pudiendo manejar aquellos eventos pertenecientes a la comunicación entrante, saliente o a ambos por igual. De esta manera es posible, por ejemplo, definir codificadores de caracteres sólo para aquellos mensajes enviados desde el servidor al cliente, permitiendo su transformación hacia las cadenas de bytes enviadas utilizando el protocolo TCP, entre otros.

\section{Modelo de controladores de canales}

En base a las posibilidades que provee Netty, se definió un modelo de controladores de canales basado en capas con el propósito de desarrollar un módulo proveedor de servicios para una arquitectura SOA, uno de los elementos bases de este trabajo. Estas capas tendrán por objetivo asegurar la independencia del funcionamiento del módulo, de la definición del protocolo. La figura~\ref{fig:commpipeline} corresponde a un diagrama que representa la interacción entre estas diferentes capas.

La primera capa de comunicación definida corresponde a un decodificador configurable, un manejador de canal que se encarga de identificar el mensaje enviado por el dispositivo para luego decodificarlo en base a esa identificación. Una vez decodificado, el mensaje es canalizado a las capas superiores.

La siguiente tiene por objetivo expandir el conjunto de eventos implementados por omisión por el \textit{framework}. Uno de los eventos personalizados implementados corresponde al de la determinación del identificador del dispositivo, decodificado desde alguno de los mensajes.

La última capa que compone el pipeline de comunicación corresponde al manejador de servicios, el cual se encarga de realizar las tareas relacionadas con la generación de los diversos servicios que este módulo se encargará de proveer.

\begin{figure}[h]
   \centering
   \includegraphics[scale=0.52]{../images/comm_pipeline.eps}
   \caption{Diagrama que describe la implementación de la tubería de comunicación y su dependencia con las diferentes interfaces de configuración}
   \label{fig:commpipeline}
\end{figure}

A la fecha de entrega de la presente memoria el módulo de comunicación soporta comunicación a través del protocolo de comunicación TCP. En el futuro se espera dar soporte a otros protocolos de tipo UDP, HTTP y otros.

\section{Implementación}

El módulo de comunicación, al igual que todos los módulos que conforman el sistema podrán ser configurados a través de una interfaz de configuración XML (ejemplo de esta interfaz en el cuadro~\ref{lst:environment.xml} del apéndice A). Para ello su construcción se basó en la implementación particular de {\tt EnvironmentModule} con una implementación particular del constructor {\tt EnvironmentModuleXMLBuilder}. 

Esto permite definir la implementación de módulos de comunicación a través del archivo de configuración XML del entorno, permitiendo configurar, además de las propiedades básicas de un módulo como:

\begin{itemize}
	\item La ubicación de la interfaz del equipo.
	\item La ubicación de la interfaz de servicios.
	\item La ubicación de la clase con la definición de los decodificadores.
	\item La ubicación de la clase con la definición de los codificadores.
	\item La ubicación de la clase con la definición de los analizadores.
\end{itemize}

La definición de decodificadores, codificadores y analizadores corresponden a clases implementadas por el usuario, que contienen métodos estáticos con parámetros previamente definidos por el sistema.
