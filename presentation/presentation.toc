\select@language {spanish}
\beamer@endinputifotherversion {3.24pt}
\select@language {spanish}
\beamer@sectionintoc {1}{Introducci\IeC {\'o}n}{3}{0}{1}
\beamer@sectionintoc {2}{Estado del Arte}{20}{0}{2}
\beamer@sectionintoc {3}{Arquitectura de la soluci\IeC {\'o}n}{31}{0}{3}
\beamer@sectionintoc {4}{Framework para el desarrollo modular}{46}{0}{4}
\beamer@sectionintoc {5}{M\IeC {\'o}dulo de mensajer\IeC {\'\i }a}{68}{0}{5}
\beamer@sectionintoc {6}{M\IeC {\'o}dulo de comunicaci\IeC {\'o}n}{79}{0}{6}
\beamer@sectionintoc {7}{M\IeC {\'o}dulo consumidor de servicios}{83}{0}{7}
\beamer@sectionintoc {8}{Validaci\IeC {\'o}n y conclusiones}{89}{0}{8}
\beamer@sectionintoc {9}{Trabajo Futuro}{98}{0}{9}
